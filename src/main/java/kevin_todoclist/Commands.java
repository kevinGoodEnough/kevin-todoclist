/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kevin_todoclist;

import java.util.Scanner;

/**
 *
 * @author afpa
 */
public class Commands {

    public static void append(Scanner scan, String[] toDoList) {

        //check the to-do list
        int count = Help.counter(toDoList);

        //check if the to-do list is full
        if (toDoList[toDoList.length - 1] != null) {
            System.out.println("Cannot add any more items to the list");
        } else {

            //add user input and trim it int the to-do list
            String item = Help.userStringInput(scan, "Enter an item: ");
            toDoList[count] = item;

            System.out.println("Item added");
        }
    }

    public static void addHere(Scanner scan, String[] toDoList) {
        //check the to-do list
        int count = Help.counter(toDoList);

        //check if the to-do list is full
        if (toDoList[toDoList.length - 1] != null) {
            System.out.println("Cannot insert a new item, the list is already full");

        } else if (toDoList[0] == null) {

            append(scan, toDoList);

        } else {
            
            //ask input index and check if it's an int, catch the exception
            int insert = Help.userIntInput(scan, toDoList, "Select where to insert an item: ");
            
            //check if the selected index is a valid one, if so send error message
            if (insert > count || insert < 0 || toDoList[insert] == null) {
                System.out.println("The selected index [" + (insert + 1) + "] is invalid");
            } else {

                String input = Help.userStringInput(scan, "Enter an item: ");

                //shift the list from an index for insert a new item in it
                for (int i = count; i > (insert); i--) {
                    if (toDoList[i] == null && toDoList[i - 1] != null) {
                        toDoList[i] = toDoList[i - 1];
                        toDoList[i - 1] = null;
                    }
                }
                toDoList[insert] = input;
                
                //Tidy up the list.
                Help.toArrange(toDoList);
            }
        }
    }

    public static void swap(Scanner scan, String[] toDoList) {

        int count = Help.counter(toDoList);
        String stock = null;
        
        //check is the list is empty, if so send error message
        if (toDoList[1] == null) {
            
            System.out.println("Cannot swap less than 2 items");
            
        //check if the list contain one item, if so swap index 0 and 1
        //with the help of the String stock
        } else if (toDoList[2] == null) {

            System.out.println("Swapped elements [1] and [2]");

            stock = toDoList[0];
            toDoList[0] = toDoList[1];
            toDoList[1] = stock;

        } else {

            int itemSelected1 = Help.userIntInput(scan, toDoList, "Select first item: ");

            if (itemSelected1 >= count || itemSelected1 < 0) {
                System.out.println("The selected index [" + (itemSelected1 + 1) + "] is invalid");
            } else {

                int itemSelected2 = Help.userIntInput(scan, toDoList, "Select second item: ");

                if (itemSelected2 >= count || itemSelected2 < 0) {
                    System.out.println("The selected index [" + (itemSelected2 + 1) + "] is invalid");
                } else {

                    stock = toDoList[itemSelected1];
                    toDoList[itemSelected1] = toDoList[itemSelected2];
                    toDoList[itemSelected2] = stock;

                    System.out.println("Swapped elements [" + (itemSelected1 + 1)
                            + "] and [" + (itemSelected2 + 1) + "]");
                }
            }
        }
    }

    public static void delete(Scanner scan, String[] toDoList) {

        int count = Help.counter(toDoList);

        //check is the list is empty, if so send error message
        if (toDoList[0] == null) {
            System.out.println("No item to delete");
        } else {
            
            //if there's only one item in the list, delete it
            if (toDoList[1] == null) {

                System.out.println("Item deleted " + "[" + toDoList[0] + "]");
                toDoList[0] = null;

            } else {

                int del = Help.userIntInput(scan, toDoList,
                        "Choose the item to delete (between " + "1 and " + count + "): ");

                if (del >= count || del < 0 || toDoList[del] == null) {
                    System.out.println("Invalid selection " + "["
                            + (del + 1) + "]" + ", cannot delete");
                } else {
                    System.out.println("Item deleted " + "[" + toDoList[del] + "]");
                    toDoList[del] = null;
                }
            }
        }
        Help.toArrange(toDoList); //
    }

    //Print the menu when user press h or help
    public static void help() {

        System.out.println("List of commands:\n"
                + "\t append|a    Add a new item to the To-Do List\n"
                + "\t insert|i    Insert a new item at a given index\n"
                + "\t swap|s      Swap two items\n"
                + "\t delete|d    Delete an item\n"
                + "\t help|h      Print this help\n"
                + "\t quit|q      Quit todoclist\n");
    }
}

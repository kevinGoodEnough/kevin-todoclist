/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kevin_todoclist;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author afpa
 */
public class Help {

    //print nothing to print when the list is empty, if not print the list
    public static void nothingToPrint(String[] toDoList) {

        int count = counter(toDoList);

        if (toDoList[0] == null) {
            System.out.println("Nothing to print");
        } else {

            System.out.println("Current To-Do List (" + count + " item(s) on " + toDoList.length + ")");

            for (int i = 0; i < toDoList.length; i++) {
                if (toDoList[i] != null) {
                    System.out.println("\t" + (i + 1) + " -> " + toDoList[i]);

                }
            }
        }
    }

    public static void toArrange(String[] toDoList) {

        for (int i = 0; i < toDoList.length - 1; i++) {
            if (toDoList[i] == null && toDoList[i + 1] != null) {

                toDoList[i] = toDoList[i + 1];
                toDoList[i + 1] = null;
            }
        }
    }

    public static int counter(String[] toDoList) {

        int count = 0;

        for (int i = 0; i < toDoList.length; i++) {
            if (toDoList[i] != null) {
                count++;
            }
        }
        return count;
    }

    //ask input index and check if it's an int, catch the exception
    public static int userIntInput(Scanner scan, String[] toDoList, String prompt) {

        
        boolean isInt = false;
        int intValue = 0;
        System.out.print(prompt);

        do {

            try {

                intValue = scan.nextInt() - 1;
                isInt = true;

            //if user input is not an int, send error message
            } catch (InputMismatchException e) {
                System.out.print("This is not an integer, please try again : ");

            //clear enter(?)
            } finally {
                scan.nextLine();
            }
        } while (isInt != true);

        return intValue;
    }

    //ask String input and trim it
    public static String userStringInput(Scanner scan, String prompt) {

        System.out.print(prompt);
        String input = scan.nextLine();

        return input.trim();
    }
}

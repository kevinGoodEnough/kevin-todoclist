/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kevin_todoclist;

import java.util.Scanner;

/**
 *
 * @author afpa
 */
public class Kevin_ToDoClist {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        menu();
    }

    public static void menu() {

        //Initialization of the scanner, the to-do list, and of a boolean to control the exit of the loop
        String[] toDoList = new String[]{null, null, null};

        //Quit when true
        boolean quit = false;

        //openning new scanner
        Scanner scan = new Scanner(System.in);

        do {

            //Print Nothing to print, or items if they exist.
            Help.nothingToPrint(toDoList);

            //Ask to the user a command to type and convert to lower case
            String input = Help.userStringInput(scan, "Command ('h' for help): ").toLowerCase();

            switch (input) {

                case "a":
                case "append":

                    //Check if the to-do list is full and print the information if so.
                    //Otherwise add item to the to-do list.
                    Commands.append(scan, toDoList);
                    break;

                case "i":
                case "insert":

                    //Check if the to-do list is full and print the information if so.
                    //Otherwise add item where the user ask it.
                    Commands.addHere(scan, toDoList);
                    break;

                case "s":
                case "swap":
                    //Check if the to-do list is empty or with only one item and print the information if so.
                    //Otherwise swap item where the user ask it.
                    Commands.swap(scan, toDoList);
                    break;

                case "d":
                case "delete":
                    //Check if the to-do list is empty and print the information if so.
                    //Delete the first item of the list if theres is only one in it.
                    //Otherwise swap item selected by the user.
                    Commands.delete(scan, toDoList);
                    break;

                case "h":
                case "help":
                    //print help info
                    Commands.help();
                    break;

                case "q":
                case "quit":
                    //leave the to-do list.
                    System.out.println("Quitting");
                    quit = true;
                    break;

                default:
                    //if the user input is not a case, print message error.
                    System.out.println("Could not parse the command");
                    break;
            }
        } while (quit == false);
        scan.close();
    }
}
